# Mopius Coding Challenge



## Aufgabe
Erstellen Sie ein App Projekt mit den Technologien Ihrer Wahl (Android oder iOS).

Fragen und parsen Sie damit das JSON File unter https://gitlab.com/BernhardA/mopius-coding-challenge/-/blob/main/json/mopius_coding_challenge_data.json und stellen Sie dessen Inhalt wie in folgendem Design dar https://gitlab.com/BernhardA/mopius-coding-challenge/-/blob/main/mopius_coding_challenge_design.png

Nehmen Sie sich nicht länger als zwei Stunden dafür Zeit. Sollten Sie nicht innerhalb dieser Zeit fertig werden, ist das kein Problem. Die Aufgabe ist nicht darauf ausgelegt alles zu erledigen. Arbeiten Sie dafür sauber und nach gängigen Coding- und Plattform Standards.

Übermitteln Sie bitte Ihre Ergebnisse in Form des Quellcode-Projektes.
